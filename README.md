# talend_practice_3

 Pour le monitoring on a besoin d'indicateurs ETL pour vérifier l’accuracy et la finalisation des données durant le process ETL par exemple : 



-Volume de données : pour  suivre le volume de données extraites, transformées et chargées au cours du processus ETL.
-Finalisation des données :  mesure le pourcentage de données chargées avec succès dans la base de données cible ou l'entrepôt de données.
-Précision des données :  mesure le pourcentage de données transformées et chargées avec précision dans la base de données cible ou l'entrepôt de données.
-Temps d'exécution : mesure le temps nécessaire pour réaliser chaque étape du processus ETL, y compris l'extraction, la transformation et le chargement des données.
Taux d'erreur : indique le nombre d'erreurs et d'avertissements survenant au cours du processus ETL.
Utilisation des ressources : suit les ressources utilisées pendant le processus ETL, y compris l'utilisation de l'unité centrale, de la mémoire et de l'espace disque.

Sur talend,  j’ai crée 3 tables dans la section Stast & Logs :

Stats Table : stocke les temps de début et de fin du travail dans le fichier stats_file.txt
Logs Table : stocke les messages d'erreur dans le fichier logs_file.txt
Meter Table : enregistre le nombre de lignes pour chaque flux dans le fichier meter_file.txt 

J'ai utilisé aussi les composants sur talend studio:tFlowMeter,tWarn,tlogCatcher,tFlowMeterCatcher,tChronometerStart,tChronometerStop pour faire le monitoring. 

